from ap_decorators import trace
@trace
def somme_rec (a:int,b:int)->int:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : n>=0
    Exemple(s) :
    $$$ somme_rec(1,1)
    2
    $$$ somme_rec(2,1)
    3

    """
    if a>0:
        res=somme_rec((a-1),b)+1
    elif a==0:
        res=b
    return res


@trace
def binomial (n:int,p:int):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ binomial(4,2)
    6

    """
    res=0
    if p==0 or n==p:
        res=1
    else:
        res=binomial(n-1,p)+binomial(n-1,p-1)
    return res

def is_palindromic(chaine):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ is_palindromic('elle')
    True
    """
    if len(chaine)<1:
        return True
    elif chaine[0]==chaine[-1] :
        return  is_palindromic(chaine[1:-1])
    else:
        return False
   
    

