#ANAGRAMME
#Nathan MOUPENDE

                        #Methode split:
#1)

# s= "la méthode split est parfois bien utile"

# s.split (' ')=['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# 
# s.split('e')=['la méthod', ' split ', 'st parfois bi', 'n util', '']
# 
# s.split('é')=['la m', 'thode split est parfois bien utile']
# 
# s.split()=['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# 
# s.split('')= Cela affiche une erreur car le séparateur est vide
# 
# s.split('split')=['la méthode ', ' est parfois bien utile']


#2)La methode split consiste a creer une liste tout en séparant  les élements

#3)OUI, cette méthode modifie la liste


                        #Methode join:

# s= "la methode split est parfois bien utile"
# 
# l=s.split()
# 
# "".join(l)= 'lamethodesplitestparfoisbienutile'
# 
# " ".join(l)='la methode split est parfois bien utile'
# 
# ";".join(l)='la;methode;split;est;parfois;bien;utile'
# 
# " tralala ".join(l)='la tralala methode tralala split tralala est tralala parfois tralala bien tralala utile'
# 
# print ("\n".join(l))=
# la
# methode
# split
# est
# parfois
# bien
# utile
# 
# "".join(s)= 'la methode split est parfois bien utile'
# 
# "!".join(s)='l!a! !m!e!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'
# 
# "".join()= Cela déclenche une erreur car ici la fonction join ne pocède pas un argument
# 
# "".join([])=''
# 
# "".join([1,2])= Cela déclenche une erreur car la fonction join ne peut pas prendre  a la fois des entiers et une chaine de caractère

#2) La fonction join consiste à coller les chaines de caractère

#3) Non elle ne modifie pas la chaine

#4)
 
# def join (ch:str,  l:list)->str:
#     """renvoie une chaîne de caractères construite
#     en concaténant toutes les chaînes de l en intercalant s sans utiliser la méthode homonyme.
# 
# 
#     Précondition : 
#     Exemple(s) :
#     $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
#     'raymond.calbuth.ronchin.fr'
# 
#     """
#     res=''
#     for elt in l:
#       
#    
#fonction sort

def sort(s: str) -> str:
    """
    Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.

    Précondition: aucune

    Exemples:

    $$$ sort('timoleon')
    'eilmnoot'
    """
    res=list(s)
    res.sort()
    return "".join(res)
    
    
def sont_anagrammes(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    False
    """
    return sort(s1)==sort(s2)
   
# 
def occurrences(chaine: str) -> dict[str, int]:
    """
    renvoie le dictionnaire des associations <lettre, nombre d'occurrences de lettre dans la chaîne>

    Précondition: aucune
    """
    res = {}
    for lettre in chaine:
        res[lettre] = res.get(lettre, 0) + 1
    return res
# 
def sont_anagrammes2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes2('orange', 'organe')
    True
    $$$ sont_anagrammes2('orange','Organe')
    False
    """
    return occurrences(s1)==occurrences(s2)

def occurrence2(chaine: str) -> list[tuple[str, int]]:
    """
    renvoie la liste des couples (lettre, nombre d' occurrences de lettre) dans la chaîne

    Précondition: aucune
    """
    res = []
    for lettre in set(chaine):
        res.append((lettre, chaine.count(lettre)))
    return res
#    
def sont_anagrammes3(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes3('orange', 'organe')
    True
    $$$ sont_anagrammes3('orange','Organe')
    False
    """
    return occurrence2(s1)==occurrence2(s2)

def minuscule(ch:str)->str:
    """renvoie chaine en lettre minuscule et sans majuscule

    Précondition :
    Exemple(s) :
    $$$ minuscule('OrangE')
    'orange'
    """
    c=''
    for elt in ch:
        c+=elt.lower()
    return c

# EQUIV_NON_ACCENTUE={'à':a, 'â':a,'ä':a,'é':e,'è':e,'ê':e, 'ë':e, 'ï':i,'î':i,'ô':O 'ö':o,'ù':u,'û':u,'ü':u,'ÿ':y,'ç':c}
def bas_casse_sans_accent(s:str)->str:
    """renvoie la chaine en minuscule et sans caractères accentuée

    Précondition :
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    """
    chaine=minuscule(s)
    c=''
    for elt in chaine:
        if elt in EQUIV_NON_ACCENTUE:
            c=c+EQUIV_NON_ACCENTUE[elt]
        else:
            c=c+elt
    return c

def sont_anagrammes(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    True
    """
    chaine1=bas_casse_sans_accent(s1)
    chaine2=bas_casse_sans_accent(s2)
    return verifie_occurence(chaine1,chaine2)

from lexique import LEXIQUE

def recherche_anagramme(mot:str)->list[str]:
    """ Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.
    """
    liste=[]
    for elt in LEXIQUE:
        if sont_anagrammes(mot,elt):
            liste.append(elt)
    return liste


