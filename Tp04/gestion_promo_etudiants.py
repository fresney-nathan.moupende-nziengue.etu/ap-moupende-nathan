from date import Date
from etudiant import Etudiant
def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]==True:
        i=i+1
    return i==len(seq_bool)


def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    i=0
    while i< len(seq_bool) and seq_bool[i]==False:
        i=i+1
    if i<len(seq_bool):
        res=True
    else:
        res=False
    return res

def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

#Q2
L_ETUDIANTS=charge_fichier_etudiants('etudiants.csv')
COURTE_LISTE=[L_ETUDIANTS[i] for i in range(0,10)]


#Q3
def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de fiches d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    return (type(x)==list)and all(isinstance (x[i],Etudiant) for i in range(0,len(x)))
#Q4)
NBRE_ETUDIANT=len (L_ETUDIANTS) #603

#Q5)
NIP=42319480
#Etienne Olivier ->337
#Q6
L_moinsvingtans=[L_ETUDIANTS[i] for i in range (0, len(L_ETUDIANTS)) if Date (2,2,2004)<L_ETUDIANTS [i].naissance]

#Q7)
def ensemble_des_formations(liste: list[Etudiant]) -> dict:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    return set(liste[i].formation for i in range (0, len(liste)))

#Q8)
    def nom_pas_judicieux(liste):
        res = {}
        for etud in liste:
            prenom = etud.prenom
            if prenom in res:
                res[prenom] = res[prenom] + 1
            else:
                res[prenom] = 1
        return res
