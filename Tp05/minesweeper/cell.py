#!Nathan MOUPENDE
# 22/02/24

"""
:mod:`cell` module

:author: 

:date: 


"""
    
class Cell:
    
    def __init__(self):
        """
        initialize a new hidden cell of a minesweeper's grid.
        existence of a bomb, number of bombs in neighborhood
        have to be stated later.
        
        precondition: none
        Examples:

        $$$ cel = Cell()
        $$$ cel.is_bomb
        False
        $$$ cel.is_revealed
        False
        $$$ cel.nbombs_in_neighborhood
        0
        """
        self.is_bomb =False
        self.is_revealed=False
        self.nbombs_in_neighborhood=0

    def incr_number_of_bombs_in_neighborhood(self):
        """
        :return: None
        :side effect: increment the number of bombs in neighborhood of self
        precondition: none
        Examples:

        $$$ cel = Cell()
        $$$ cel.nbombs_in_neighborhood
        0
        $$$ cel.incr_number_of_bombs_in_neighborhood()
        $$$ cel.nbombs_in_neighborhood
        1
        """
        self.nbombs_in_neighborhood+=1

    def reveal(self):
        """
        modify reveal state of self
        precondition: none
        Examples:

        $$$ cel = Cell()
        $$$ cel.is_revealed
        False
        $$$ cel.reveal()
        $$$ cel.is_revealed
        True
        """
        self.is_revealed=True

    def set_bomb(self):
        """
        put a bomb in self 

        precondition: none
        Examples:

        $$$ cel = Cell()
        $$$ cel.is_bomb
        False
        $$$ cel.set_bomb()
        $$$ cel.is_bomb
        True
        """
        self.is_bomb=True

    def __str__(self):
        """
        :return: a string representation of self state
        :rtype: str
        precondition: none
        Examples:
        
        $$$ cel = Cell()
        $$$ str(cel) == ' '
        True
        $$$ cel.reveal()
        $$$ str(cel) == '0'
        True
        $$$ cel.incr_number_of_bombs_in_neighborhood()
        $$$ str(cel) == '1'
        True
        $$$ cel.set_bomb()
        $$$ str(cel) == 'B'
        True
        """
        if self.is_revealed:
            if self.is_bomb:
                res="B"
            else:
                res=str(self.nbombs_in_neighborhood)
        else:
            res=' '
        return res 
    




if (__name__ == '__main__'):
    import apl1test
    apl1test.testmod('cell.py')

